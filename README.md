# Ghost Timeline

☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

v1.0.0: Timeline representation of Ghost posts, with primary tag navigation

## Demo
You can see this project running [on my personal blog here](https://chronology.feralresearch.org/).

## K8 / Docker Configuration

Image: `registry.gitlab.com/feralresearch/fr-ghost-timeline:latest`

- Get a content api key (see below)
- Create a config object and UUencode it (see below)
- Deploy k8 (sample config in `deploy/k8`)
- UPDATE YOUR SECRET

## Local Manual Configuration

- You will need to get a **content api key** and api URL via a [custom integration](https://ghost.org/integrations/custom-integrations/) from your Ghost install admin control panel. There is no need to keep this secret, per the Ghost docs: "Content API keys are provided via a query parameter in the url. These keys are safe for use in browsers and other insecure environments"

- Create a configuration JSON that resembles the following (note the content key is part of the endpoint url):

  ```js
  {
  endpoint:
      "https://EXAMPLE.COM/ghost/api/v3/content/posts/?key=CONTENT_API_KEY&include=tags&limit=all",
  title: "My Website",
  titleLink: "https://EXAMPLE.COM/",
  useCache: false,
  navigation: [
      { name: "Everything", tag: "all" },
      { name: "News", tag: "news" },
      { name: "Software", tag: "software" },
      { name: "Cooking", tag: "cooking" },
  ],
  }
  ```

  ### Option 1: Simple Hardcoded

  Clone this repo,
  edit `www/assets/index.js` and assign an object like the above to the `config` variable. Done. Optionally you may remove env.sh and all references to env.js.

  ### Option 2: UUEncode for Cloud Support

  Docker containers are easily configured by .env variables. Adapting [this technique](https://medium.freecodecamp.org/how-to-implement-runtime-environment-variables-with-create-react-app-docker-and-nginx-7f9d42a91d70), we can provide our JS with "environment" vars and this makes it easy to configure static websites with .env, however it's difficult to put an JSON in a .env var... unless it's UUEncoded. Thus, UUEncode the config JSON and provide a .env file with the following key: `APP_CONFIG=`

  ```js
  One-Liner encoder / decoder:

  config = {...}
  encoded = btoa(JSON.stringify(config))
  decoded = JSON.parse(atob(encoded))
  ```

## Development (useCache)

There is a config option `useCache.` If this is set to `true`, the script will load the posts API once and subsequently use only the cached copy. This is useful for development when you might want to reload a lot but not hammer your Ghost instance, but for production it should always be set to `false.`
