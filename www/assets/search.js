window.addEventListener("load", () => {
  window.addEventListener("timelineLoaded", () => {
    buildSearchBox("navigation");
  });
});
const keyListener = (e) => {
  if (e.key === "Escape") {
    document.getElementById("searchInput").value = "";
    document.getElementById("search").style.display = "none";
  }
};
const doSearch = (e) => {
  const searchResults = document.getElementById("searchResults");
  searchResults.innerHTML = "";
  const searchText = e.target.value?.toLowerCase();
  const posts = JSON.parse(localStorage.getItem("posts"))?.posts;
  if (searchText) {
    const results = posts.filter(
      (post) =>
        post.title.toLowerCase().includes(searchText) ||
        post.tags
          .map((tag) => tag.name)
          .join()
          .includes(searchText)
    );
    searchResults.appendChild(
      _createDivWithClass(
        "count",
        `${results.length} ${results.length === 1 ? "result" : "results"}`
      )
    );
    results.forEach((result) => {
      const resultEl = _createDivWithClass("result", result.title);
      resultEl.appendChild(
        _createDivWithClass(
          "tags",
          result.tags.map((tag) => tag.name).join(", ")
        )
      );
      resultEl.onclick = () => (document.location.href = result.url);
      searchResults.appendChild(resultEl);
    });
  }
};
const buildSearchBox = (root) => {
  const search = _createDivWithId("search");
  const searchResults = _createDivWithId("searchResults");
  const searchInput = _createDivWithId("searchInput");
  const input = document.createElement("input");
  const icon = document.createElement("div");
  icon.className = "icon fa fa-search";
  input.id = "searchInput";
  input.addEventListener("input", doSearch);
  input.addEventListener("keydown", keyListener);
  input.type = "text";
  searchInput.appendChild(input);
  searchInput.appendChild(icon);
  search.appendChild(searchInput);
  search.appendChild(searchResults);
  document.getElementById(root).appendChild(search);
  document.getElementById("search").style.display = "none";
};
