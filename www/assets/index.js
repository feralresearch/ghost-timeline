const config = JSON.parse(atob(window._env_.APP_CONFIG));
window.addEventListener("load", () => onLoad());
window.addEventListener("onclick", () => {
  if (document.getElementById("burger")?.getBoundingClientRect().width > 0)
    document.getElementById("navItems").style.display = "none";
});
window.onresize = () => {
  document.getElementById("search").style.display = "none";
  if (document.getElementById("burger").getBoundingClientRect().width === 0)
    document.getElementById("navItems").style.display = "flex";
  _updateTerminatorMask();
};

const onLoad = (tag, callDepth = 0) => {
  const cachedPosts = JSON.parse(localStorage.getItem("posts"))?.posts;
  if ((config.useCache && cachedPosts) || callDepth > 0) {
    const items = document.getElementById("items");
    items.innerHTML = "";
    const req = _parseQueryVars();
    localStorage.setItem("filterBy", tag ? tag : req.t ? req.t : "all");
    let filteredPosts = [...cachedPosts];
    let filterBy = localStorage.getItem("filterBy");
    if (filterBy && filterBy !== "all") {
      filteredPosts = cachedPosts.filter((post) => {
        const tagList = post.tags.map((tag) => tag.name).join(",");
        return tagList.includes(filterBy) ? post : null;
      });
    }
    if (filteredPosts)
      filteredPosts.forEach((post, idx) => {
        const item = addItem(post);
        item.classList.add(idx % 2 ? "right" : "left");
      });
    _updateTerminatorMask();
    document.getElementById("timeline").style.opacity =
      filteredPosts.length === 0 ? 0 : 1;
    buildNavigation();
    document.body.classList.add("visible");
    const event = new CustomEvent("timelineLoaded");
    window.dispatchEvent(event);
  } else {
    _loadData(config.endpoint, (data) => {
      localStorage.setItem("posts", JSON.stringify(data));
      onLoad(tag, callDepth + 1);
    });
  }
};

// Adds item to the timeline
const addItem = ({ title, published_at, feature_image, tags, url }) => {
  const items = document.getElementById("items");
  let item = _createDivWithClass("item");
  let marker = _createDivWithClass("marker");
  let markerText = _createDivWithClass(
    "markerText",
    _dateFormat(new Date(published_at))
  );
  let markerTitle = _createDivWithClass("markerTitle", title);
  let tagList = tags?.length > 0 ? tags.map((tag) => tag.name).join(", ") : "";
  tagList = tagList
    .split(",")
    .filter((tag) => tag !== localStorage.getItem("filterBy"))
    .join(",");
  let markerTags = _createDivWithClass("markerTags", tagList);
  markerText.appendChild(markerTitle);
  markerText.appendChild(markerTags);
  marker.appendChild(markerText);
  item.appendChild(marker);
  items.appendChild(item);
  item.style.backgroundImage = `url(${feature_image})`;
  item.onclick = () => (document.location.href = url);
  return item;
};

const buildNavigation = () => {
  const navbar = document.getElementById("navigation");
  navbar.innerHTML = "";
  const burger = navbar.appendChild(_createDivWithId("burger", "="));
  burger.onclick = (e) => {
    e.stopPropagation();
    document.getElementById("navItems").style.display =
      document.getElementById("navItems").style.display === "flex"
        ? "none"
        : "flex";
  };

  const navTitle = _createDivWithClass("navTitle", config.title);
  navTitle.onclick = () => (document.location.href = config.titleLink);
  navbar.appendChild(navTitle);
  const navItems = _createDivWithId("navItems");
  navbar.appendChild(navItems);
  config.navigation.forEach((menuItem) => {
    link = _createDivWithClass("navItem", menuItem.name);
    if (menuItem.tag === localStorage.getItem("filterBy")) {
      navbar.appendChild(_createDivWithId("filterTitle", menuItem.name));
      link.classList.add("selected");
    }
    link.onclick = () => onLoad(menuItem.tag);
    navItems.appendChild(link);
  });
  const icon = document.createElement("div");
  icon.addEventListener("click", () => {
    document.getElementById("search").style.display =
      document.getElementById("search").style.display === "none"
        ? "flex"
        : "none";
    if (document.getElementById("burger")?.getBoundingClientRect().width > 0)
      document.getElementById("navItems").style.display = "none";
  });
  icon.className = "fa fa-search";
  navItems.appendChild(icon);
};

// Helper methods
const _loadData = (url, cb) => {
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      cb(data);
    })
    .catch((error) => console.error(error));
};

const _createDivWithClass = (className, innerText) => {
  let div = document.createElement("div");
  div.className = className;
  if (innerText) div.innerText = innerText;
  return div;
};

const _createDivWithId = (idName, innerText) => {
  let div = document.createElement("div");
  div.id = idName;
  if (innerText) div.innerText = innerText;
  return div;
};

const _parseQueryVars = () => {
  const query = window.location.search.substring(1).split("&");
  const vars = {};
  if (query.length > 0)
    query.forEach((pair) => {
      const key = pair.split("=")[0];
      const val = pair.split("=")[1];
      vars[key] = val;
    });
  return vars;
};

const _getPageHeight = () => {
  //https://stackoverflow.com/questions/1145850/how-to-get-height-of-entire-document-with-javascript
  const body = document.body;
  const html = document.documentElement;
  return Math.max(
    body.scrollHeight,
    body.offsetHeight,
    html.clientHeight,
    html.scrollHeight,
    html.offsetHeight
  );
};

const _updateTerminatorMask = () => {
  const terminalMarkerRect = document
    .getElementsByClassName("marker")
    [
      document.getElementsByClassName("marker").length - 1
    ]?.getBoundingClientRect();
  const maskHeight = _getPageHeight() - terminalMarkerRect?.top;
  document.getElementById("bottomMask").style.height = `${maskHeight}px`;
};

const _dateFormat = (date) => {
  const monthLut = {
    0: "January",
    1: "February",
    2: "March",
    3: "April",
    4: "May",
    5: "June",
    6: "July",
    7: "August",
    8: "September",
    9: "October",
    10: "November",
    11: "December",
  };
  return `${monthLut[date.getMonth()]} ${date.getFullYear()}`;
};
